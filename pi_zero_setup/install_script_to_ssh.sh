#! /bin/bash
# Copy the script to pi
# ssh-keygen -f "/home/jp/.ssh/known_hosts" -R "<hostname without .local>"
# scp pi_zero_setup/install_script_to_ssh.sh jp@zerowhblue1:~

echo 'alias ll="ls -lAh " ' >> ~/.bashrc
echo 'alias sv="source .venv/bin/activate " ' >> ~/.bashrc


sudo apt update
sudo apt upgrade -y
sudo apt autoremove -y

mkdir ~/software

sudo apt install -y htop tmux btop git 

# oh-my-tmux
mkdir -p ~/software/oh-my-tmux
git clone https://github.com/gpakosz/.tmux.git ~/software/oh-my-tmux/
mkdir -p ~/.config/tmux
ln -s ~/software/oh-my-tmux/.tmux.conf ~/.config/tmux/tmux.conf
cp ~/software/oh-my-tmux/.tmux.conf.local ~/.config/tmux/tmux.conf.local
