#! /bin/bash

# Define the desired Python version
DESIRED_VERSION="3.12.4"

echo 'alias ll="ls -lAh " ' >> ~/.bashrc
echo 'alias sv="source .venv/bin/activate " ' >> ~/.bashrc

# fix legacy key warning 
# source: https://d14znet.github.io/blog-chirpy/posts/legacy-keyring-warning/
sudo apt-key export 90FDDD2E | sudo gpg --dearmour -o /etc/apt/trusted.gpg.d/***raspberrypi-archive-stable***.gpg



sudo apt update
sudo apt upgrade -y
sudo apt autoremove -y

mkdir ~/software

# install pb user and dev tools
sudo apt install -y htop tmux btop git 

# increase swap size to 2GB
# Reason: so that the pi does not get stuck during compilation
sudo sed -i 's/CONF_SWAPSIZE=.*/CONF_SWAPSIZE=2048/' /etc/dphys-swapfile
sudo systemctl restart dphys-swapfile

# oh-my-tmux
mkdir -p ~/software/oh-my-tmux
git clone https://github.com/gpakosz/.tmux.git ~/software/oh-my-tmux/
mkdir -p ~/.config/tmux
ln -s ~/software/oh-my-tmux/.tmux.conf ~/.config/tmux/tmux.conf
cp ~/software/oh-my-tmux/.tmux.conf.local ~/.config/tmux/tmux.conf.local
# activate mouse
sed -i '/set -g mouse/c\set -g mouse on' ~/.config/tmux/tmux.conf.local && tmux source-file ~/.config/tmux/tmux.conf.local



# Python
# Check if Python is installed and get the version
if PYTHON_VERSION=$(python3 --version 2>/dev/null); then
    # Extract the version number
    PYTHON_VERSION_NUMBER=$(echo $PYTHON_VERSION | awk '{print $2}')
    
    # Check if the installed version matches the desired version
    if [ "$PYTHON_VERSION_NUMBER" == "$DESIRED_VERSION" ]; then
        echo "Python version $DESIRED_VERSION is already installed. No action needed."
    else
        echo "Python version $DESIRED_VERSION is not installed. Current version: $PYTHON_VERSION_NUMBER"
        # You can add commands here to install the desired version or take other actions
        # clean install (compilation) of a specific python version
        # sudo apt purge python* -y
            # wget -qO - https://raw.githubusercontent.com/tvdsluijs/sh-python-installer/main/python.sh | sudo bash -s 3.11.3
        wget -qO - https://raw.githubusercontent.com/tvdsluijs/sh-python-installer/main/python.sh | sudo bash -s 3.12.4
    fi
else
    echo "Python is not installed."
    # You can add commands here to install Python
    wget -qO - https://raw.githubusercontent.com/tvdsluijs/sh-python-installer/main/python.sh | sudo bash -s 3.11.3
fi


# to fix apt errors after custom compiled python installation
sudo apt remove python3-apt -y
sudo apt autoremove -y
sudo apt autoclean -y
sudo apt install python3-apt -y

# install more dependencies
sudo apt update
sudo apt install -y adduser libfontconfig1 cmake gettext

# poetry
curl -sSL https://install.python-poetry.org | python3 -
poetry completions bash >> ~/.bash_completion
# echo 'export PATH="$HOME/.local/bin:$PATH"' >> ~/.bashrc
# exec "$SHELL" # restart shell
poetry config virtualenvs.in-project true # dev env setup



# rust
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

# lazygit
# wget https://github.com/jesseduffield/lazygit/releases/download/v0.37.0/lazygit_0.37.0_Linux_arm64.tar.gz
wget https://github.com/jesseduffield/lazygit/releases/download/v0.43.1/lazygit_0.43.1_Linux_armv6.tar.gz
# tar xzvf lazygit_0.37.0_Linux_arm64.tar.gz
tar xzvf lazygit_0.43.1_Linux_armv6.tar.gz
sudo mv lazygit /usr/bin/
rm LICENSE README.md
lazygit -v

# test to build nodejs
sudo apt install g++ make
# wget https://nodejs.org/dist/v20.16.0/node-v20.16.0.tar.gz -P ~/software/
wget https://nodejs.org/dist/v18.20.4/node-v18.20.4.tar.gz -P ~/software/
cd ~/software
tar xzvf ~/software/node-v20.16.0.tar.gz
cd node-v20.16.0
./configure
make -j2
make install

# test to use an unofficial nodejs build
cd ~/software
wget https://unofficial-builds.nodejs.org/download/release/v21.7.3/node-v21.7.3-linux-armv6l.tar.xz
tar -xJf node-v21.7.3-linux-armv6l.tar.xz
cd node-v21.7.3-linux-armv6l
sudo cp -R * /usr/local/

mkdir ~/.npm-global
npm config set prefix '~/.npm-global'
echo 'export PATH=~/.npm-global/bin:$PATH' >> ~/.profile

node -v
npm -v


# nerdfont
cd ~/software
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.3.3/RobotoMono.zip
unzip RobotoMono.zip 
sudo mv R* /usr/share/fonts 
rm LICENSE.txt readme.md 

# install neovim
cd ~/software
wget https://github.com/neovim/neovim/archive/refs/tags/stable.tar.gz
tar xzvf stable.tar.gz
cd neovim-stable
make CMAKE_BUILD_TYPE=RelWithDebInfo
sudo make install
# test: maybe make install already copies the binary to the right places
# sudo cp ./build/bin/nvim /usr/bin




#LunarVim
LV_BRANCH='release-1.4/neovim-0.9' bash <(curl -s https://raw.githubusercontent.com/LunarVim/LunarVim/release-1.4/neovim-0.9/utils/installer/install.sh)

#go

#gotop
cd ~/software
git clone https://github.com/xxxserxxx/gotop.git
cd gotop
# This ugly SOB gets a usable version from the git tag list
VERS="$(git tag -l --sort=-v:refname | sed 's/v\([^-].*\)/\1/g' | head -1 | tr -d '-' ).$(git describe --long --tags | sed 's/\([^-].*\)-\([0-9]*\)-\(g.*\)/r\2.\3/g' | tr -d '-')"
DAT=$(date +%Y%m%dT%H%M%S)
go build -o gotop \
	-ldflags "-X main.Version=v${VERS} -X main.BuildDate=${DAT}" \
	./cmd/gotop
cp gotop /usr/bin/



# =========================================
# ========= Plantus Botus Setup ===========
# =========================================


# install InfluxDB 1.6.7
# sudo apt install -y influxdb
sudo apt install -y influxdb-client

# 2024-08-8: I found 1.7.11 form armhf
# https://community.influxdata.com/t/influxdb-1-7-11-download-links/18898
# install instructions from: https://docs.influxdata.com/influxdb/v2/install/
cd ~/software
# wget https://dl.influxdata.com/influxdb/releases/influxdb-1.7.11_linux_armhf.tar.gz 
# tar xvfz influxdb-1.7.11_linux_armhf.tar.gz
# sudo cp ./influxdb-1.7.11-1/usr/bin/influxd /usr/local/bin/

# 2024-08-8: I found 1.8.10 for armhf
# https://www.influxdata.com/downloads/
wget https://dl.influxdata.com/influxdb/releases/influxdb-1.8.10_linux_armhf.tar.gz
tar xvfz influxdb-1.8.10_linux_armhf.tar.gz
sudo cp ./influxdb-1.8.10-1/usr/bin/influxd /usr/local/bin/


# test installation with
influxd version


# install Grafana
cd ~/software
sudo apt install musl -y
# wget https://dl.grafana.com/enterprise/release/grafana-enterprise-rpi_9.4.7_armhf.deb
# sudo dpkg -i grafana-enterprise-rpi_9.4.7_armhf.deb
# see: https://grafana.com/grafana/download/10.1.7?platform=arm
wget https://dl.grafana.com/enterprise/release/grafana-enterprise-rpi_10.1.7_armhf.deb
sudo dpkg -i grafana-enterprise-rpi_10.1.7_armhf.deb
sudo apt update
sudo apt install grafana-enterprise-rpi -y
sudo service grafana-server start
sudo update-rc.d grafana-server defaults

# the script mentiones the following commands to start the systems on start
# sudo /bin/systemctl daemon-reload
# sudo /bin/systemctl enable grafana-server



# install python packages
# sudo apt install python3-pip  # already party of the python install script
# 2024-08-08: I will use poetry for package management --> see pyproject.toml
# pip install -U pip
# pip install influxdb pytest 

echo 'export PATH="$HOME/.local/bin:$PATH"' >> ~/.bashrc
source .bashrc 




# Extras

# Log2Ram
# https://pimylifeup.com/raspberry-pi-log2ram/
sudo apt install rsync
cd ~/software
wget https://github.com/azlux/log2ram/archive/master.tar.gz -O log2ram.tar.gz
tar xf log2ram.tar.gz
cd log2ram-master
sudo ./install.sh
# sudo reboot
sudo sed -i 's/SIZE=128M/SIZE=40M/' /etc/log2ram.conf



# fastfetch
# https://github.com/fastfetch-cli/fastfetch
# https://github.com/fastfetch-cli/fastfetch/wiki/Building
cd ~/software
wget https://github.com/fastfetch-cli/fastfetch/archive/refs/tags/2.21.0.tar.gz
tar xvfz 2.21.0.tar.gz
cd fastfetch-2.21.0
mkdir -p build
cd build
cmake ..
cmake --build . --target fastfetch --target flashfetch
sudo cp fastfetch /usr/local/bin/