#!/bin/bash

# Variables
REMOTE_USER="jp"
REMOTE_HOST="zerowhblack.local"
TMUX_SESSION="ssh_infinity"
INTERVAL=30  # Time interval between reconnections in seconds (e.g., 3600s = 1 hour)

# Function to connect and reattach tmux session
connect_and_attach() {
    ssh -t $REMOTE_USER@$REMOTE_HOST "tmux attach-session -t $TMUX_SESSION || tmux new-session -s $TMUX_SESSION"
}

while true; do
    echo "Connecting to $REMOTE_USER@$REMOTE_HOST..."
    connect_and_attach
    echo "Connection closed. Reconnecting in $INTERVAL seconds..."
    sleep $INTERVAL
done

