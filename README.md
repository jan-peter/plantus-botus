# Plantus Botus

The automated plant watering and monitoring system.
The idea is to have a capable small, lightweight system, that takes care of your watering needs, to grow kitchen herbs or what ever else you want.

![System Overview](https://gitlab.com/jan-peter/plantus-botus/-/raw/main/docs/plantus_botus.png)

## Functionality
- automated plant watering
    - dedicated support for special plants
- plant monitoring
- growth images -> timelaps -> gif/video

# Measurements
| Parameter | Sensor | Comment |
| --- | --- | ---|
| Soil humidity | sensor | WIP |
| CO2 | ENS160+AHT21 | to be tested |
| CO2 PPM | SCD-40 | to be tested, [Photo-Acoustic vs NDIR](https://www.airgradient.com/blog/co2-sensors-photo-acoustic-vs-ndir-updated/) |
| DC power monitoring | INA219 | to be tested |
| Air Temp & Humidity | HDC3021  | [Adafruit Blog](https://learn.adafruit.com/adafruit-hdc3021-precision-temperature-humidity-sensor)  better than the  DHT11/DHT22  |
| --- | --- | --- |
| ADC & I2C \multiplexer | ADS1115 |



# Usage of AI
- to learn what a good set of parameters look like
    - multiple sensors -> many parameters
    - clues in images/taken photos -> 


All runs on a Raspberry Pi Zero WH.

Built with
- Grafana ~~8.3.4~~ 10.1.7 [Grafana Download Page](https://grafana.com/grafana/download/10.1.7?platform=arm)
- InlfuxDB 1.6.4
- RPi.gpio
- smbus2
- argparse
- logging
- pytest


# Install
on the pi:
``` bash
git clone git@gitlab.com:jan-peter/plantus-botus.git
cd plantus-botus
poetry install

```


# Development
``` bash
poetry install --with dev

# if you get a poetry lock error:
poetry lock
```
