"""
Module for testing the main plantus botus script
"""
import pytest
import plantus_botus as pb

# @pytest.mark.skip
def test_create_env():
    env = pb.Environment()
    assert env.status["location"] == "kitchen"


def test_create_plant():
    plant = pb.Plant(species="herb", name="basil")
    assert len(plant.status) == 4


def test_plant_change_location():
    plant = pb.Plant(species="herb", name="basil")
    plant.change_location("balcony")
    assert plant.status["environment"].status["location"] == "balcony"


def test_pb_main(capsys):
    assert pb.main(["-d"])
    out, err = capsys.readouterr()
    assert out == "Debug mode is on."
    assert err == ""
