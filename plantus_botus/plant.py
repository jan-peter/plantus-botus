
class Plant:
    """A Plant object is just another mouth to water."""

    def __init__(self, species: str, name: str, environment_name:str, garden:Garden) -> None:
        """Standard definition of a digital copy of a plant.

        Args:
            species (str): eg herb, flower, houseplant, vegetable, fruit, other
            name (str): eg basil, rosemary, mango, ..
            location (str): kitchen, livingroom, bath_dill, bath_chili
            team_name (str): if multipe plants are standing together, they are bounded like a team
                             like "toaster"

        Returns:
            None
        """
        self.garden = garden
        self.possible_locations = []

        self.status = {}
        self.status["plant_species"] = species
        self.status["plant_name"] = name
        self.status["plant_ID"] = self.id_generator()
        self.status["environment_name"] = environment_name
        self.update_status()

    def change_location(self, new_location: str):
        """If you want to relocate your plant."""

        self.status["environment"].status["location"] = new_location

    def update_status(self):
        """Update status information of a plant."""

        # find fitting environment in the garden's environment list
        for env in self.garden.environments:
            if env.status["name"] == self.status["environment_name"]:
                 self.status["env_status"] = env.get_status()


    def get_status(self) -> dict:
        """Return status information of a plant."""
        return self.status["env_status"]

    def id_generator(self) -> str:
        """Generator to create the plant ID.
        1. character: species starting letter
        2. character: name starting letter
        3. character: dash
        rest: hash of creation time
        """

        # create ID
        id = ""
        id = str(id + self.status["plant_species"][:1])
        id = str(id + self.status["plant_name"][:1])
        id += "-"

        date_now = datetime.now()
        date_now_enc = str(date_now).encode("UTF-8")
        hash = hashlib.md5()
        hash.update(date_now_enc)
        id = id + str(int(hash.hexdigest(), 16))[0:7]

        return id
