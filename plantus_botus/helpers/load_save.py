# helpers functin to load and save garden infos

import toml
import logging
from pathlib import Path
from typing import List, Dict, Union


def save_to_toml(dict:Dict, file_path: Union[str, Path]) -> None:
    """Saves the Garden configuration to a TOML file.

    Params:
        dict (Dict): The Garden configuration as a dictionary.
        file_path (Union[str, Path]): The path to the file to save the configuration to.
    """
    toml_content = dict
    path = Path(file_path).expanduser()
    path.parent.mkdir(parents=True, exist_ok=True)
    with open(path, 'w') as file:
        toml.dump(toml_content, file)
    logging.info(f"Garden configuration saved to {file_path}")


def load_from_toml(file_path: Union[str, Path]) -> Dict:
    """Loads the content from a TOML file.

    Params:
        file_path (Union[str, Path]): The path to the file to load the configuration from.

    Returns:
        Dict: The toml content as a dictionary.
    """
    path = Path(file_path).expanduser()
    if not path.exists():
        logging.error(f"File {file_path} does not exist.")
        return {}
    with open(path, 'r') as file:
        toml_content:dict = toml.load(file)
    logging.info(f"TOML content loaded from {file_path}")
    return toml_content


def update_toml(file_path: Union[str, Path], updates: Dict) -> None:
    """Updates the Garden configuration in a TOML file.

    Params:
        file_path (Union[str, Path]): The path to the file to update the configuration in.
        updates (Dict): The updates to apply to the configuration.
    """
    # Load existing content
    toml_content = load_from_toml(file_path)
    
    # Update the content with the new settings
    toml_content.update(updates)
    
    # Save the updated content back to the file
    save_to_toml(toml_content, file_path)
    
    logging.info(f"Updated information in {file_path}")

