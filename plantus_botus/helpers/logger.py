# managing the settings of the logger

import logging

# Set the logger mode - options: "debug", "info", "warning", "error", "critical"
logger_mode:str = "debug"

def create_logger(mode="info"):
    """Function to create the logger equipped with the right mode.

    Loggin Levels: https://docs.python.org/3/library/logging.html#logging-levels
    - DEBUG: Detailed information, typically useful only for diagnosing problems.
    - INFO: Confirmation that things are working as expected.
    - WARNING: An indication that something unexpected happened or indicative of some problem in the near future.
    - ERROR: Due to a more serious problem, the software has not been able to perform some function.
    - CRITICAL: A very serious error, indicating that the program itself may be unable to continue running.

    Params:
        mode (str): The mode for the logger. Valid values are "debug" and "warning".

    Returns:
        logger (logging.Logger): The configured logger object.

    """
    if mode == "debug":
        LOG_FORMAT = "%(levelname)s %(asctime)s - %(message)s"
        logging.basicConfig(
            filename="plantus_botus.log",
            format=LOG_FORMAT,
            filemode="w",
            level=logging.DEBUG,
        )
        logger = logging.getLogger(name="Plantus Botus")
        logger.debug("Entering debug mode.")
        logger.info("Starting the logger in INFO Mode.")
        logger.info(f"Logger name: {logger.name}")
        logger.info(f"Logging file location: {logger.handlers[0].baseFilename}")

    if mode == "info":
        LOG_FORMAT = "%(levelname)s %(asctime)s - %(message)s"
        logging.basicConfig(
            filename="plantus_botus.log",
            format=LOG_FORMAT,
            filemode="w",
            level=logging.INFO,
        )
        logger = logging.getLogger(name="Plantus Botus")
        logger.info("Starting the logger in INFO Mode.")
        logger.info(f"Logger name: {logger.name}")
        logger.info(f"Logging file location: {logger.handlers[0].baseFilename}")

    return logger

Logger = create_logger(mode=logger_mode)