# WIP file for make an init system check during boot

import platform
import shutil
import subprocess
from typing import Dict, List

import psutil
import RPi.GPIO as GPIO

from plantus_botus.helpers.logger import Logger


def get_cpu_info() -> Dict[str, str]:
    """Retrieve information about the CPU.

    Returns:
        dict: A dictionary containing CPU information.
    """
    cpu_info = {
        "Physical cores": str(psutil.cpu_count(logical=False)),
        "Total cores": str(psutil.cpu_count(logical=True)),
        "Max Frequency": f"{psutil.cpu_freq().max:.2f}Mhz",
        "Current Frequency": f"{psutil.cpu_freq().current:.2f}Mhz",
        "CPU Usage": f"{psutil.cpu_percent(interval=1)}%"
    }
    return cpu_info


def get_cpu_temperature() -> str:
    """Retrieve the current CPU temperature.

    Returns:
        str: The CPU temperature in degrees Celsius.
    """
    try:
        temps = psutil.sensors_temperatures()
        cpu_temps = temps.get("cpu-thermal", [])
        if cpu_temps:
            return f"{cpu_temps[0].current}°C"
        else:
            return "Temperature data not available"
    except AttributeError:
        return "Temperature sensors not supported"


def get_memory_info() -> Dict[str, str]:
    """Retrieve information about the memory usage.

    Returns:
        dict: A dictionary containing memory information.
    """
    memory = psutil.virtual_memory()
    memory_info = {
        "Total": f"{memory.total / (1024 ** 3):.2f} GB",
        "Available": f"{memory.available / (1024 ** 3):.2f} GB",
        "Used": f"{memory.used / (1024 ** 3):.2f} GB",
        "Percentage": f"{memory.percent}%"
    }
    return memory_info


def get_disk_usage() -> Dict[str, str]:
    """Retrieve disk usage information.

    Returns:
        Dict[str, str]: A dictionary containing disk usage information with keys:
                        - 'Total': Total disk space in GB.
                        - 'Used': Used disk space in GB.
                        - 'Free': Free disk space in GB.
                        - 'Percentage': Percentage of disk space used.
    """
    disk_usage = psutil.disk_usage('/')
    disk_info = {
        "Total": f"{disk_usage.total / (1024 ** 3):.2f} GB",
        "Used": f"{disk_usage.used / (1024 ** 3):.2f} GB",
        "Free": f"{disk_usage.free / (1024 ** 3):.2f} GB",
        "Percentage": f"{disk_usage.percent}%"
    }
    return disk_info


def get_swap_usage() -> Dict[str, str]:
    """
    Retrieve information about the swap memory usage.

    Returns:
        Dict[str, str]: A dictionary containing swap memory information with keys:
                        - 'Total': Total swap space in GB.
                        - 'Used': Used swap space in GB.
                        - 'Free': Free swap space in GB.
                        - 'Percentage': Percentage of swap space used.
    """
    swap = psutil.swap_memory()
    swap_info = {
        "Total": f"{swap.total / (1024 ** 3):.2f} GB",
        "Used": f"{swap.used / (1024 ** 3):.2f} GB",
        "Free": f"{swap.free / (1024 ** 3):.2f} GB",
        "Percentage": f"{swap.percent}%"
    }
    return swap_info


def get_network_interfaces() -> Dict[str, List[str]]:
    """
    Retrieve information about network interfaces.

    Returns:
        Dict[str, List[str]]: A dictionary containing network interface information.
                              The key is the interface name, and the value is a list of strings
                              describing each address associated with the interface.
                              Each address string includes the address family and the actual address.
    """
    interfaces = psutil.net_if_addrs()
    interface_info = {}
    for interface, addrs in interfaces.items():
        interface_info[interface] = [f"{addr.family.name}: {addr.address}" for addr in addrs]
    return interface_info


def check_network_latency(host: str = "8.8.8.8") -> Dict[str, str]:
    """
    Check network latency to a specified host.

    Parameters:
        host (str): The host to ping. Defaults to Google's DNS server 8.8.8.8.

    Returns:
        Dict[str, str]: A dictionary containing the ping results. Includes:
                        - 'Host': The host being pinged.
                        - 'Output': The ping command output, or an error message if the ping fails.
    """
    try:
        result = subprocess.run(['ping', '-c', '4', host], capture_output=True, text=True)
        return {
            "Host": host,
            "Output": result.stdout
        }
    except subprocess.CalledProcessError as e:
        return {
            "Host": host,
            "Output": f"Error checking network latency: {e}"
        }


def check_filesystem_integrity() -> Dict[str, str]:
    """
    Check filesystem integrity for errors or corruption.

    Returns:
        Dict[str, str]: A dictionary containing the result of the filesystem check. Includes:
                        - 'Status': The status of the filesystem check.
                        - 'Output': The output from the dmesg command or an error message.
    """
    try:
        result = subprocess.run(['dmesg', '--level=err'], capture_output=True, text=True)
        return {
            "Status": "Filesystem check complete",
            "Output": result.stdout
        }
    except subprocess.CalledProcessError as e:
        return {
            "Status": "Error",
            "Output": f"Error checking filesystem integrity: {e}"
        }


def check_sd_card_health() -> Dict[str, str]:
    """
    Check SD card health by evaluating free space and total capacity.

    Returns:
        Dict[str, str]: A dictionary containing SD card health information. Includes:
                        - 'Total': Total SD card space in GB.
                        - 'Used': Used SD card space in GB.
                        - 'Free': Free SD card space in GB.
    """
    try:
        total, used, free = shutil.disk_usage('/')
        sd_health = {
            "Total": f"{total / (1024 ** 3):.2f} GB",
            "Used": f"{used / (1024 ** 3):.2f} GB",
            "Free": f"{free / (1024 ** 3):.2f} GB"
        }
        return sd_health
    except Exception as e:
        return {
            "Error": f"Error checking SD card health: {e}"
        }


def check_power_supply() -> Dict[str, str]:
    """
    Check the status of the power supply.

    Returns:
        Dict[str, str]: A dictionary indicating the power supply status. Includes:
                        - 'Status': A description of the power supply status or an error message.
                        - 'Details': The detailed output from the vcgencmd command or error details.
    """
    try:
        result = subprocess.run(['vcgencmd', 'get_throttled'], capture_output=True, text=True)
        return {
            "Status": "Power supply check complete",
            "Details": result.stdout.strip()
        }
    except subprocess.CalledProcessError as e:
        return {
            "Status": "Error",
            "Details": f"Error checking power supply: {e}"
        }


def list_usb_devices() -> Dict[str, str]:
    """
    List connected USB devices.

    Returns:
        Dict[str, str]: A dictionary containing the USB devices information. Includes:
                        - 'Status': The status of the USB device check.
                        - 'Devices': The output listing connected USB devices, or an error message.
    """
    try:
        result = subprocess.run(['lsusb'], capture_output=True, text=True)
        return {
            "Status": "USB device list retrieved",
            "Devices": result.stdout.strip()
        }
    except subprocess.CalledProcessError as e:
        return {
            "Status": "Error",
            "Devices": f"Error listing USB devices: {e}"
        }
   

def check_i2c_devices() -> Dict[str, str]:
    """Check for connected I2C devices.

    Returns:
        Dict[str, str]: A dictionary containing I2C device information. Includes:
                        - 'Status': The status of the I2C device check.
                        - 'Devices': The output from the i2cdetect command, or an error message.
    """
    try:
        result = subprocess.run(['i2cdetect', '-y', '1'], capture_output=True, text=True)
        return {
            "Status": "I2C device check complete",
            "Devices": result.stdout.strip()
        }
    except subprocess.CalledProcessError as e:
        return {
            "Status": "Error",
            "Devices": f"Error checking I2C devices: {e}"
        }


def check_gpio_status() -> Dict[str, str]:
    """
    Check the status of GPIO pins using BOARD pin numbering.

    Returns:
        Dict[str, str]: A dictionary containing GPIO pin status information. Includes:
                        - 'Pin': The physical GPIO pin number.
                        - 'Status': The status of the pin (High/Low) or an error message.
    """
    # Set GPIO mode to BCM or BOARD  numbering
    # https://raspberrypi.stackexchange.com/questions/12966/what-is-the-difference-between-board-and-bcm-for-gpio-pin-numbering
    # GPIO.setmode(GPIO.BCM)  # addressing a pin by GPIO number => GPIO2
    GPIO.setmode(GPIO.BOARD)  # addressing a pin by pin number => pin 3
    
    gpio_status = {}
    for pin in range(1, 41):  # Physical pin numbers on the 40-pin header
        try:
            GPIO.setup(pin, GPIO.IN)
            status = GPIO.input(pin)
            gpio_status[f"GPIO {pin}"] = "High" if status else "Low"
        except RuntimeError as e:
            gpio_status[f"GPIO {pin}"] = f"Error ({e})"
    GPIO.cleanup()
    return gpio_status


def get_boot_logs() -> Dict[str, str]:
    """
    Retrieve the boot logs for errors or warnings.

    Returns:
        Dict[str, str]: A dictionary containing boot log information. Includes:
                        - 'Status': The status of the boot log retrieval.
                        - 'Logs': The boot log output from the dmesg command, or an error message.
    """
    try:
        result = subprocess.run(['dmesg'], capture_output=True, text=True)
        return {
            "Status": "Boot logs retrieved",
            "Logs": result.stdout.strip()
        }
    except subprocess.CalledProcessError as e:
        return {
            "Status": "Error",
            "Logs": f"Error retrieving boot logs: {e}"
        }


def check_os_updates() -> Dict[str, str]:
    """
    Check for available OS updates on a Raspberry Pi running Raspberry Pi OS based on Debian Bookworm (32-bit).

    Returns:
        Dict[str, str]: A dictionary containing the OS update check information. Includes:
                        - 'Status': The status of the OS update check.
                        - 'UpdateInfo': The result of the 'apt-get update' command.
                        - 'UpgradeInfo': The preview result of the 'apt-get upgrade -s' command, or an error message.
    """
    try:
        # Check for available updates
        result_update = subprocess.run(['sudo', 'apt-get', 'update'], capture_output=True, text=True)
        update_info = result_update.stdout
        
        # Preview available upgrades
        result_upgrade = subprocess.run(['sudo', 'apt-get', 'upgrade', '-s'], capture_output=True, text=True)
        upgrade_info = result_upgrade.stdout
        
        return {
            "Status": "OS update check complete",
            "UpdateInfo": update_info.strip(),
            "UpgradeInfo": upgrade_info.strip()
        }
    except subprocess.CalledProcessError as e:
        return {
            "Status": "Error",
            "UpdateInfo": "",
            "UpgradeInfo": f"Error checking for OS updates: {e}"
        }




def init_check() -> None:
    """
    Perform a hardware check and print the results.
    """

    Logger.info("Hardware Check for Raspberry Pi Zero WH")

    print("Hardware Check for Raspberry Pi Zero WH\n")
    print(f"System: {platform.system()} {platform.release()} {platform.version()}")
    print(f"Processor: {platform.processor()}\n")
    
    print("CPU Information:")
    cpu_info = get_cpu_info()
    for key, value in cpu_info.items():
        print(f"{key}: {value}")

    print("\nMemory Information:")
    memory_info = get_memory_info()
    for key, value in memory_info.items():
        print(f"{key}: {value}")

    print("\nDisk Usage:")
    disk_usage_info = get_disk_usage()
    for key, value in disk_usage_info.items():
        print(f"{key}: {value}")

    print("\nCPU Temperature:")
    print(get_cpu_temperature())

    print("\nSwap Usage:")
    swap_usage_info = get_swap_usage()
    for key, value in swap_usage_info.items():
        print(f"{key}: {value}")

    print("\nNetwork Interfaces:")
    network_interfaces_info = get_network_interfaces()
    for interface, details in network_interfaces_info.items():
        print(f"Interface: {interface}")
        for detail in details:
            print(f"  {detail}")

    print("\nFilesystem Integrity Check:")
    filesystem_integrity_info = check_filesystem_integrity()
    for key, value in filesystem_integrity_info.items():
        print(f"{key}: {value}")

    print("\nSD Card Health Check:")
    sd_card_health_info = check_sd_card_health()
    for key, value in sd_card_health_info.items():
        print(f"{key}: {value}")

    print("\nPower Supply Check:")
    power_supply_info = check_power_supply()
    for key, value in power_supply_info.items():
        print(f"{key}: {value}")

    print("\nUSB Devices:")
    usb_devices_info = list_usb_devices()
    for key, value in usb_devices_info.items():
        print(f"{key}: {value}")

    print("\nI2C Devices:")
    i2c_devices_info = check_i2c_devices()
    for key, value in i2c_devices_info.items():
        print(f"{key}: {value}")

    print("\nGPIO Status:")
    gpio_status_info = check_gpio_status()
    for pin, status in gpio_status_info.items():
        print(f"{pin}: {status}")

    print("\nBoot Logs:")
    boot_logs_info = get_boot_logs()
    for key, value in boot_logs_info.items():
        print(f"{key}: {value}")

    print("\nOS Updates:")
    os_update_info = check_os_updates()
    for key, value in os_update_info.items():
        print(f"{key}: {value}")



if __name__ == "__main__":
    main()
