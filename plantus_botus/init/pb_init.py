# This file is used to initialize the bot and set up the necessary variables

from datetime import datetime
from pathlib import Path
import toml

from plantus_botus.helpers.load_save import save_to_toml, load_from_toml
from plantus_botus.helpers.logger import Logger

DEV_MODE: bool = True  # indicates that the software is in development mode

if DEV_MODE:
    Logger.debug("Dev-Mode detected. Using example settings.")
    SETTINGS_PATH: Path = Path(__file__).parent / "docs" / "example_settings.toml"
    GARDEN_PATH: Path = Path(__file__).parent / "docs" / "example_garde.toml"
    
    Logger.debug(f"Settings path: {SETTINGS_PATH}")
    Logger.debug(f"Garden path: {GARDEN_PATH}")
    # LOGGER_MODE = "debug"  # set in logger.py
    FIRST_USE: bool|None = None
if not DEV_MODE:
    SETTINGS_PATH: Path = Path.home() / ".config" / "plantus_botus" / "settings.toml"
    GARDEN_PATH: Path = Path.home() / ".config" / "plantus_botus" / "garden.toml"
    # LOGGER_MODE = "debug"  # set in logger.py
    FIRST_USE: bool|None = None



def creat_or_load_settings() -> dict:
    """Create or load the settings file for the bot.
    Wheter the settings file exists or not, indicates, if this is the
    first use of the bot.
    
    Returns:
        dict: The settings for plantus botus.
    """

    # if settings.toml not exists in ~/.config/plantus_botus, create it
    if not SETTINGS_PATH.exists():
        FIRST_USE = True

        SETTINGS_PATH.parent.mkdir(parents=True, exist_ok=True)
        
        settings: dict = {
            # "created": datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            "created": datetime.now().isoformat(),
            "garden_path": GARDEN_PATH,
            "stations": {
                "base_001": {
                }
            } 
        }

        # write settings to disc
        save_to_toml(settings, SETTINGS_PATH)
        Logger.info(f"New settings file created at {SETTINGS_PATH}")
        return settings

    else:
        FIRST_USE = False
        settings: dict = load_from_toml(SETTINGS_PATH)
        return settings


def setup(first_use: bool=FIRST_USE) -> None:
    """Set up the bot for the first use.
    
    Args:
        first_use (bool): If this is the first use of the bot.
    """

    logger = Logger
    if first_use:
        print("Welcome to plantus botus! Let's set up your garden.") 
        Logger.info("No settings file found. Creating new settings")
        
        settings = creat_or_load_settings()
        print(settings)
    else:
        Logger.info(f"Settings file found in {SETTINGS_PATH}. Loading settings ..")
        print("Welcome back to plantus botus!")

        from plantus_botus.init.pi_init import init_check
        init_check()
    
