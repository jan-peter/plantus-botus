# Everything related to the garden

from datetime import datetime
import logging


class Garden:
    """A Garden object is a collection of plants."""

    def __init__(self, name: str, logger: logging, rpi:Pi) -> None:
        self.rpi = rpi
        self.status = {}
        self.status["name"] = name
        self.status["id"] = self.id_generator()
        self.status["plants"] = []
        self.status["plant_counter"] = 0
        self.logger = logger
        self.environments = []
        self.add_environment(environment=Environment(sensor_measurements=self.rpi.get_sensorhub_data()))


    def add_environment(self, environment: Environment()):
        """Add an environment to the garden."""
        self.environments.append(environment)


    def add_plant(self) -> None:
        """Function to add a plant to the garden."""

        print("\nLets add a new plant to the garden.")

        p_species = input("--> Herb or regular plant?: ")
        if p_species == "":
            p_species = "herb"

        p_name = input("--> Name of the plant. (Basil, Olive tree, ..): ")
        if p_name == "":
            p_name = "basil"

        # print all possible locations known to the garden
        print("\n--> Please choose the location of the plant.")
        print("\tName (Location)")
        for env in self.environments:
            print(f"    * {env.status['name']} ({env.status['location']})")
        p_env = input("Location name of the plant (homebase, ..): ")

        new_plant = Plant(name=p_name, species=p_species,
                          environment_name=p_env, garden=self)
        self.status[f"plant_{self.status['plant_counter']}"] = new_plant.status
        self.status["plants"].append(new_plant)
        self.status["plant_counter"] = len(self.status["plants"])

        self.logger.info("New plant added.")


    def get_status(self) -> dict:
        """Return status information of a plant."""
        return self.status
    

    def id_generator(self) -> str:
        """Generator to create the garden ID.
        1. character: name starting letter
        2. character: dash
        rest: hash of creation time
        """

        # create ID
        id = ""
        id = str(id + self.status["name"][:1])
        id += "-"

        date_now = datetime.now()
        date_now_enc = str(date_now).encode("UTF-8")
        hash = hashlib.md5()
        hash.update(date_now_enc)
        id = id + str(int(hash.hexdigest(), 16))[0:7]

        return id


    def remove_plant(self, plant: Plant):
        """Remove a plant from the garden."""
        self.status["plants"].remove(plant)



    def update_status(self):
        """Update status information of a plant."""
        # self.status["soil_humidity"] = get_soil_humidity()
        pass

