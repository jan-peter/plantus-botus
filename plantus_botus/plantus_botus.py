from __future__ import annotations
# import hashlib
from pathlib import Path
# from pprint import pprint
from typing import Any

import argparse
import logging
# import subprocess
from datetime import datetime
import json
from http import client  # for type annotations <3.10
from time import sleep

# import RPi.GPIO as GPIO
# import smbus2 as smbus
# from influxdb import InfluxDBClient
# from psutil import cpu_freq, cpu_percent, disk_partitions, disk_usage, virtual_memory

# from psutil import disk_usage


# LEDPin = 11  # RPI Board pin11
# RELAY_PIN = 11

# # SMBus object -> Filled in setup()
# bus = None

# # SensorHub constants
# DEVICE_BUS = 1
# DEVICE_ADDR = 0x17

# TEMP_REG = 0x01
# LIGHT_REG_L = 0x02
# LIGHT_REG_H = 0x03
# STATUS_REG = 0x04
# ON_BOARD_TEMP_REG = 0x05
# ON_BOARD_HUMIDITY_REG = 0x06
# ON_BOARD_SENSOR_ERROR = 0x07
# BMP280_TEMP_REG = 0x08
# BMP280_PRESSURE_REG_L = 0x09
# BMP280_PRESSURE_REG_M = 0x0A
# BMP280_PRESSURE_REG_H = 0x0B
# BMP280_STATUS = 0x0C
# HUMAN_DETECT = 0x0D


status = {}
status_pi = {}
status_env = {}

# ==========================================
# ==========================================

# https://github.com/AlexisGomes/JsonEncoder/blob/master/JsonEncoder.py

# class JSONEncoder(json.JSONEncoder):
#     def default(self, obj):
#         if hasattr(obj, serialize):
#             return obj.serialize()


class JSONEncoder(json.JSONEncoder):
    """Encoding from class objects"""

    # overload method default
    def default(self, obj):

        # Match all the types you want to handle in your converter
        if isinstance(obj, Environment):
            return {"environment_status": obj.status}
        elif isinstance(obj, Plant):
            return {"plant_status": obj.status}
        elif isinstance(obj, PlantusBotus):
            return {"plantusbotus_status": obj.status}
        elif isinstance(obj, Pi):
            return {"pi_status": obj.status}

        # Call the default method for other types
        return json.JSONEncoder.default(self, obj)


class JSONDecoder(json.JSONDecoder):
    def __init__(self, *args, **kwargs):
        json.JSONDecoder.__init__(self, object_hook=self.object_hook, *args, **kwargs)

    def object_hook(self, obj):

        # handle your custom classes
        if isinstance(obj, dict):
            if "status" in obj:  # and "value2" in obj:
                print(obj)
                return Environment(obj.get("status"))  # , obj.get("value_2"))

        # handling the resolution of nested objects
        if isinstance(obj, dict):
            for key in list(obj):
                obj[key] = self.object_hook(obj[key])
            return obj

        if isinstance(obj, list):
            for i in range(0, len(obj)):
                obj[i] = self.object_hook(obj[i])
            return obj

        # resolving simple strings objects
        # dates
        if isinstance(obj, str):
            obj = self._extract_date(obj)
        return obj


def json_encode(data, **kwargs: Any) -> str:
    return JSONEncoder(**kwargs).encode(data)


def json_decode(string):
    return JSONDecoder().decode(string)


# ==========================================
# ==========================================


# class Environment:
#     """Information about the local ambient situation"""

#     def __init__(self, sensor_measurements:dict, location:str=None, name:str=None):
#         """Initialize the environment."""
#         if location is None:
#             location = input("Please enter a location for the environment: ")
#             if location == "":
#                 location = "kitchen"
#         if name is None:
#             name = input("Please enter a name for the environment: ")
#             if name == "":
#                 name = "homebase"
        
#         self.status = {}
#         self.status["name"] = name
#         self.status["location"] = location
#         self.status["id"] = self.id_generator()

#         self.status["air_humidity"] = None
#         self.status["air_temperature"] = None
#         self.status["barometer_temperature"] = None
#         self.status["barometer_pressure"] = None
#         self.status["brightness"] = None
#         self.update_env_measurements(sensor_measurements=sensor_measurements)

#     def id_generator(self) -> str:
#         """Generator to create the plant ID.
#         1. character: species starting letter
#         2. character: name starting letter
#         3. character: dash
#         rest: hash of creation time
#         """

#         # create ID
#         id = ""
#         id = str(id + self.status["name"][:2])
#         id = str(id + self.status["location"][:1])
#         id += "-"

#         date_now = datetime.now()
#         date_now_enc = str(date_now).encode("UTF-8")
#         hash = hashlib.md5()
#         hash.update(date_now_enc)
#         id = id + str(int(hash.hexdigest(), 16))[0:7]

#         return id
    
#     def get_status(self) -> dict:
#         return {"air_humidity": self.status["air_humidity"],
#                 "air_temperature": self.status["air_temperature"],
#                 "barometer_pressure": self.status["barometer_pressure"],
#                 "brightness": self.status["brightness"]}
    
#     def update_env_measurements(self, sensor_measurements:dict):
#         """Update the environment measurements"""

#         self.status["air_humidity"] = sensor_measurements["air_humidity"]
#         self.status["air_temperature"] = sensor_measurements["air_temperature"]
#         self.status["barometer_temperature"] = sensor_measurements["barometer_temperature"]
#         self.status["barometer_pressure"] = sensor_measurements["barometer_pressure"]
#         self.status["brightness"] = sensor_measurements["sensor_brightness_lux"]

#         print(f"New environment measurements: {self.status}")


# class Plant:
#     """A Plant object is just another mouth to water."""

#     def __init__(self, species: str, name: str, environment_name:str, garden:Garden) -> None:
#         """Standard definition of a digital copy of a plant.

#         Args:
#             species (str): eg herb, flower, houseplant, vegetable, fruit, other
#             name (str): eg basil, rosemary, mango, ..
#             location (str): kitchen, livingroom, bath_dill, bath_chili
#             team_name (str): if multipe plants are standing together, they are bounded like a team
#                              like "toaster"

#         Returns:
#             None
#         """
#         self.garden = garden
#         self.possible_locations = []

#         self.status = {}
#         self.status["plant_species"] = species
#         self.status["plant_name"] = name
#         self.status["plant_ID"] = self.id_generator()
#         self.status["environment_name"] = environment_name
#         self.update_status()

#     def change_location(self, new_location: str):
#         """If you want to relocate your plant."""

#         self.status["environment"].status["location"] = new_location

#     def update_status(self):
#         """Update status information of a plant."""

#         # find fitting environment in the garden's environment list
#         for env in self.garden.environments:
#             if env.status["name"] == self.status["environment_name"]:
#                  self.status["env_status"] = env.get_status()


#     def get_status(self) -> dict:
#         """Return status information of a plant."""
#         return self.status["env_status"]

#     def id_generator(self) -> str:
#         """Generator to create the plant ID.
#         1. character: species starting letter
#         2. character: name starting letter
#         3. character: dash
#         rest: hash of creation time
#         """

#         # create ID
#         id = ""
#         id = str(id + self.status["plant_species"][:1])
#         id = str(id + self.status["plant_name"][:1])
#         id += "-"

#         date_now = datetime.now()
#         date_now_enc = str(date_now).encode("UTF-8")
#         hash = hashlib.md5()
#         hash.update(date_now_enc)
#         id = id + str(int(hash.hexdigest(), 16))[0:7]

#         return id


# class Garden:
#     """A Garden object is a collection of plants."""

#     def __init__(self, name: str, logger: logging, rpi:Pi) -> None:
#         self.rpi = rpi
#         self.status = {}
#         self.status["name"] = name
#         self.status["id"] = self.id_generator()
#         self.status["plants"] = []
#         self.status["plant_counter"] = 0
#         self.logger = logger
#         self.environments = []
#         self.add_environment(environment=Environment(sensor_measurements=self.rpi.get_sensorhub_data()))

#     def id_generator(self) -> str:
#         """Generator to create the garden ID.
#         1. character: name starting letter
#         2. character: dash
#         rest: hash of creation time
#         """

#         # create ID
#         id = ""
#         id = str(id + self.status["name"][:1])
#         id += "-"

#         date_now = datetime.now()
#         date_now_enc = str(date_now).encode("UTF-8")
#         hash = hashlib.md5()
#         hash.update(date_now_enc)
#         id = id + str(int(hash.hexdigest(), 16))[0:7]

#         return id

#     def add_plant(self) -> None:
#         """Function to add a plant to the garden."""

#         print("\nLets add a new plant to the garden.")

#         p_species = input("--> Herb or regular plant?: ")
#         if p_species == "":
#             p_species = "herb"

#         p_name = input("--> Name of the plant. (Basil, Olive tree, ..): ")
#         if p_name == "":
#             p_name = "basil"

#         # print all possible locations known to the garden
#         print("\n--> Please choose the location of the plant.")
#         print("\tName (Location)")
#         for env in self.environments:
#             print(f"    * {env.status['name']} ({env.status['location']})")
#         p_env = input("Location name of the plant (homebase, ..): ")

#         new_plant = Plant(name=p_name, species=p_species,
#                           environment_name=p_env, garden=self)
#         self.status[f"plant_{self.status['plant_counter']}"] = new_plant.status
#         self.status["plants"].append(new_plant)
#         self.status["plant_counter"] = len(self.status["plants"])

#         self.logger.info("New plant added.")

#     def remove_plant(self, plant: Plant):
#         """Remove a plant from the garden."""
#         self.status["plants"].remove(plant)


#     def add_environment(self, environment: Environment()):
#         """Add an environment to the garden."""
#         self.environments.append(environment)



#     def get_status(self) -> dict:
#         """Return status information of a plant."""
#         return self.status
    

#     def update_status(self):
#         """Update status information of a plant."""
#         # self.status["soil_humidity"] = get_soil_humidity()
#         pass



# class Pi:
#     """A Pi object is responsible for everything."""

#     def __init__(self) -> None:
#         self.status = {}

#     # https://www.thepythoncode.com/article/get-hardware-system-information-python
#     def get_size(self, bytes, suffix="B"):
#         """Scale bytes to its proper format e.g:
#         1253656 => '1.20MB'
#         1253656678 => '1.17GB'
#         """

#         factor = 1024
#         for unit in ["", "K", "M", "G", "T", "P"]:
#             if bytes < factor:
#                 return f"{bytes:.2f}{unit}{suffix}"
#             bytes /= factor

#     # https://github.com/gsurma/rpi_lcd_system_monitoring/blob/master/rpi_lcd_system_monitoring.py
#     def get_temperature(self):

#         reading = subprocess.check_output(["/opt/vc/bin/vcgencmd", "measure_temp"])
#         reading_string = reading.decode()

#         return float(reading_string.split("=")[1][:-3])

#     def get_cpu_usage(self):
#         return cpu_percent(interval=0.5)

#     def get_cpu_frequency(self):
#         return cpu_freq()

#     def get_disk_usage(self):
#         partitions = disk_partitions()
#         partition = partitions[0]
#         try:
#             partition_usage = disk_usage(partition.mountpoint)
#             disk_used = self.get_size(partition_usage.used)
#             disk_free = self.get_size(partition_usage.free)
#             disk_used_percentage = partition_usage.percent
#             return disk_used, disk_free, disk_used_percentage
#         except PermissionError:
#             # this can be catched due to the disk that isn't ready
#             pass

#     def get_memory_available(self):
#         """Return the available memory in MB."""

#         svmem = virtual_memory()
#         mem_avail = self.get_size(svmem.available)
#         return mem_avail

#     def update_status(self):
#         """Update status information of the Pi."""

#         self.status["cpu temperature"] = self.get_temperature()
#         self.status["cpu usage"] = self.get_cpu_usage()

#         disk_used, disk_free, disk_used_percentage = self.get_disk_usage()
#         self.status["disk_used"] = disk_used
#         self.status["disk_free"] = disk_free
#         self.status["disk_used_percentage"] = disk_used_percentage

#         mem_avail = self.get_memory_available()
#         self.status["memory_avail"] = mem_avail

#     def get_status(self) -> dict:
#         return self.status

#     def pi_setup(self) -> None:
#         """Setting up all the right GPIO pins and bus."""

#         global bus
#         GPIO.setmode(GPIO.BOARD)
#         GPIO.setup(LEDPin, GPIO.OUT)
#         GPIO.output(LEDPin, GPIO.LOW)  # Set ledPin low to off led

#         # Numbers GPIOs by physical location
#         # Set ledPin's mode is output
#         # print('using pin%d' % LEDPin)

#         # SensorHub
#         bus = smbus.SMBus(DEVICE_BUS)

#     def destroy(self):
#         GPIO.output(LEDPin, GPIO.LOW)  # led off
#         GPIO.cleanup()  # Release resource

#     def relay(self) -> None:
#         """Switching the relay on and off for the water pump."""
#         GPIO.output(RELAY_PIN, GPIO.HIGH)  # relay on
#         print("...relay on")
#         sleep(3)  # delay 1 second
#         GPIO.output(RELAY_PIN, GPIO.LOW)  # relay off
#         print("...relay off")
#         sleep(1)  # delay 1 second

#     def blink_led(self, ledPin: int) -> None:
#         """Making an LED blinkn on GPIO Pin ledPin"""

#         GPIO.output(ledPin, GPIO.HIGH)  # led on
#         print("...led on")
#         sleep(1)  # delay 1 second
#         GPIO.output(ledPin, GPIO.LOW)  # led off
#         print("led off...")
#         sleep(1)

#     # SensorHub
#     def get_sensorhub_data(self) -> dict:
#         """Getting sensor data from the SensorHub."""

#         output = {}

#         # Setup Buffer
#         aReceiveBuf = []
#         aReceiveBuf.append(0x00)

#         # fill buffer
#         for i in range(TEMP_REG, HUMAN_DETECT + 1):
#             aReceiveBuf.append(bus.read_byte_data(DEVICE_ADDR, i))

#         output["air_temperature"] = aReceiveBuf[ON_BOARD_TEMP_REG]
#         output["air_humidity"] = aReceiveBuf[ON_BOARD_HUMIDITY_REG]
#         output["sensor_brightness_lux"] = (
#             aReceiveBuf[LIGHT_REG_H] << 8 | aReceiveBuf[LIGHT_REG_L]
#         )
#         output["barometer_temperature"] = aReceiveBuf[BMP280_TEMP_REG]
#         output["barometer_pressure"] = (
#             aReceiveBuf[BMP280_PRESSURE_REG_L]
#             | aReceiveBuf[BMP280_PRESSURE_REG_M] << 8
#             | aReceiveBuf[BMP280_PRESSURE_REG_H] << 16
#         )

#         return output


# 2022-02-19: maybe own database class?
# 2022-07-08: good idea
# class DB:
#     """DataBase class to handle DB things."""

#     def __init__(self, logger):
#         self.client = self.db_start("test", logger)

#     def db_setup(self, arg, logger):
#         """Initial setup for the DB."""

#         # Setup database
#         client = InfluxDBClient(host="localhost", port=8086)

#         # To reset the test DB
#         if arg == "reset":
#             client.drop_database("test")
#             logger.warning("A database got dropped.")
#         client.create_database("test")
#         logger.warning("A database got created.")

#     def db_start(self, desired_db_name: str, logger):
#         """Setting up the connection to the InfluxDB."""

#         client = InfluxDBClient(
#             host="localhost", port=8086, username="admin", password="admin"
#         )
#         client.switch_database(desired_db_name)
#         logger.info(f"switched database successfully to {desired_db_name}")
#         return client

#     def db_write(self, payload: list) -> None:
#         """Function to write data to the InfluxDB."""

#         # print payload for checking
#         print("\nCurrent Pi status:")
#         print(payload)

#         # write data to DB
#         self.client.write_points(payload)


# ======================
# ===== end of DB ======
# ======================


class PlantusBotus:
    """Dealing with all general questions:
    - setting up start screen
    - saving/loagin settings
    """

    def __init__(self, logger, db, rpi, garden_full_path="."):
        self.garden_full_path = garden_full_path
        self.status = {}  # dict of all garden status information
        self.status["comment"] = "Garden initialised."
        self.status["started"] = str(datetime.now())
        self.status["started"] = datetime.now().isoformat(timespec="minutes")
        self.status["plant_counter"] = 0
        self.logger = logger
        self.rpi = rpi
        self.db = db
        self.garden = Garden(logger=logger, name="Sandstraße", rpi=self.rpi)


    def init_setup(self) -> None:
        """PB init setting"""
        print(
            "Buonos Dias! Mein Name ist Plantus Botus.\n"
            "Welcome to the world of ever-growing herbs."
        )


    def safe_garden(obj) -> None:
        """Writing settings to the garden safe file."""
        encoded_obj = json_encode(obj, ensure_ascii=False, indent=4)
        path = Path("garden.json")
        path.write_text(encoded_obj, encoding="utf-8")

    def load_garden(self) -> None:
        """Reading the settings file."""
        with open(self.garden_full_path, "r") as file:
            data = file.read()
        self.garden = json.loads(data)


    def build_db_payload(self) -> list:
        """Function to build the playload for the DB."""

        ambient_sensor = self.rpi.get_sensorhub_data()
        pi_status = self.rpi.get_status()

        payload = []
        data_ambient = {
            "measurement": "ambient_sensor",
            "time": datetime.now(),
            "fields": ambient_sensor,
        }

        print("Info about all plants:", self.garden.status)

        # loop over all plants and add them to the payload in the garden dict
        for plant in self.garden.status["plants"]:
            data_plant = {
                          "measurement": f"{plant.status['plant_name']}_{plant.status['environment_name']}_{plant.status['plant_ID']}",
                          "time": datetime.now(),
                          "fields": plant.get_status()
            }
            payload.append(data_plant)

        data_pi = {"measurement": "pi_sensors",
                   "time": datetime.now(),
                   "fields": pi_status}

        payload.append(data_ambient)
        payload.append(data_pi)

        return payload

# ======================
# ===== end of PB ======
# ======================


def argument_handler(argv):
    """For handling the argument given to the program.
    argparse from YT@anthonywritescode
    """
    parser = argparse.ArgumentParser(
        description="Your friendly plant robot."
        "The following shows you what is possible",
        prog="Plantus Botus",
    )
    parser.add_argument(
        "--add", action="store_true", help="add a new plant to the system"
    )
    parser.add_argument(
        "--debug", "-d", action="store_true", help="the debug flag", dest="debug"
    )
    parser.add_argument(
        "--init", action="store_true", help="initialising the plantus botus system"
    )
    parser.add_argument(
        "--reset", "-r", action="store_true", help="resetting the whole project"
    )

    args = parser.parse_args(argv)
    return args


def create_logger(mode="warning"):
    """Function to create the logger equipped with the right mode."""

    if mode == "debug":
        LOG_FORMAT = "%(levelname)s %(asctime)s - %(message)s"
        logging.basicConfig(
            filename="plantus_botus.log",
            format=LOG_FORMAT,
            filemode="w",
            level=logging.DEBUG,
        )
        logger = logging.getLogger(name="Plantus Botus")
        logger.debug("Entering debug mode.")

    if mode == "warning":
        LOG_FORMAT = "%(levelname)s %(asctime)s - %(message)s"
        logging.basicConfig(
            filename="plantus_botus.log",
            format=LOG_FORMAT,
            filemode="w",
            level=logging.WARNING,
        )
        logger = logging.getLogger(name="Plantus Botus")
        logger.warning("Starting in Warning Mode.")

    return logger


def loop(pb: PlantusBotus, logger: logging) -> None:
    """Wrapper for the programm loop."""

    logger.info("Entering the program loop ...")

    while True:
        # blink_led(led_pin=LEDPin)
        pb.rpi.update_status()

        # update the environment measurements
        for env in pb.garden.environments:
            # when env name is "homebase"
            if env.status["name"] == "homebase":
                env.update_env_measurements(sensor_measurements=pb.rpi.get_sensorhub_data())
            else:
                raise NameError("Environment could not be updated.")

        # collect measurements
        measurements = pb.build_db_payload()


        # write measurements in DB
        pb.db.db_write(measurements)
        print("==> Written to DB.")

        # relay()
        sleep(1)
    pb.rpi.destroy()


def main(argv=None) -> None:
    """Main loop of the plantus botus system."""

    # handling any given arguments
    args = argument_handler(argv)

    # Dealing with Logging
    if args.debug == True:
        logger = create_logger(mode="debug")
    elif args.debug == False:
        logger = create_logger(mode="warning")
    logger.info("Logger created successfully")

    
    db = DB(logger=logger)  # Setting up the DB

    # Setting up Pi and its pins
    rpi = Pi()
    rpi.pi_setup()
    logger.info("Pi got created.")

    pb = PlantusBotus(db=db, rpi=rpi,
                      logger=logger)  # Setting up the PB system

    # ========= INIT =========
    # for very-first PB usage
    if args.init == True:
        pb.db.db_setup(args, logger)
        pb.init_setup()

        pb.garden.add_plant()

        pb.safe_garden()

        logger.info("Garden setup complete.")
        logger.info("=======================")

    elif args.init == False:
        pb.db.db_setup(args, logger)

    if args.add == True:
        print("This is sadly not implemented yet")



    # if args.reset == True:
    # db.db_setup(args, logger)

    client = pb.db.db_start("test", logger)



    enter = input("Entering loop? (y/n):")
    if enter in ["y", "Y", "yes", "Yes", "YES", ""]:
        loop(pb=pb, logger=logger)
    elif enter == "n":
        print("ok. leaving no ..")
    else:
        print("I don't understand.")
        rpi.destroy()


# ===================================================
# ===================================================


# program start from here
if __name__ == "__main__":

    try:
        main()
        # exit()

    # When 'Ctrl+C' is pressed
    except KeyboardInterrupt:
        pb.rpi.destroy()
