
class Environment:
    """Information about the local ambient situation"""

    def __init__(self, sensor_measurements:dict, location:str=None, name:str=None):
        """Initialize the environment."""
        if location is None:
            location = input("Please enter a location for the environment: ")
            if location == "":
                location = "kitchen"
        if name is None:
            name = input("Please enter a name for the environment: ")
            if name == "":
                name = "homebase"
        
        self.status = {}
        self.status["name"] = name
        self.status["location"] = location
        self.status["id"] = self.id_generator()

        self.status["air_humidity"] = None
        self.status["air_temperature"] = None
        self.status["barometer_temperature"] = None
        self.status["barometer_pressure"] = None
        self.status["brightness"] = None
        self.update_env_measurements(sensor_measurements=sensor_measurements)

    def id_generator(self) -> str:
        """Generator to create the plant ID.
        1. character: species starting letter
        2. character: name starting letter
        3. character: dash
        rest: hash of creation time
        """

        # create ID
        id = ""
        id = str(id + self.status["name"][:2])
        id = str(id + self.status["location"][:1])
        id += "-"

        date_now = datetime.now()
        date_now_enc = str(date_now).encode("UTF-8")
        hash = hashlib.md5()
        hash.update(date_now_enc)
        id = id + str(int(hash.hexdigest(), 16))[0:7]

        return id
    
    def get_status(self) -> dict:
        return {"air_humidity": self.status["air_humidity"],
                "air_temperature": self.status["air_temperature"],
                "barometer_pressure": self.status["barometer_pressure"],
                "brightness": self.status["brightness"]}
    
    def update_env_measurements(self, sensor_measurements:dict):
        """Update the environment measurements"""

        self.status["air_humidity"] = sensor_measurements["air_humidity"]
        self.status["air_temperature"] = sensor_measurements["air_temperature"]
        self.status["barometer_temperature"] = sensor_measurements["barometer_temperature"]
        self.status["barometer_pressure"] = sensor_measurements["barometer_pressure"]
        self.status["brightness"] = sensor_measurements["sensor_brightness_lux"]

        print(f"New environment measurements: {self.status}")
