import RPi.GPIO as GPIO

from time import sleep

LEDPin = 11  # RPI Board pin11


def setup():
    global bus
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(LEDPin, GPIO.OUT)
    GPIO.output(LEDPin, GPIO.LOW)  # Set ledPin low to off led

    # Numbers GPIOs by physical location
    # Set ledPin's mode is output
    print("using pin%d" % LEDPin)


def blink_led(LEDPin: int) -> None:
    print("\n enter blink led \n")
    GPIO.output(LEDPin, GPIO.HIGH)  # led on
    print("...led on")
    sleep(1)  # delay 1 second
    # GPIO.output(LEDPin, GPIO.LOW)  # led off
    GPIO.setup(LEDPin, GPIO.OUT)
    GPIO.output(LEDPin, GPIO.LOW)
    print("led off...")
    sleep(1)


def destroy():
    GPIO.output(LEDPin, GPIO.LOW)  # led off
    GPIO.cleanup()  # Release resource


def loop():
    global LEDPin
    while True:
        blink_led(LEDPin=LEDPin)
        sleep(1)
        print("Done.")


# ===================================================
# ===================================================


# Program start from here
if __name__ == "__main__":
    setup()
    try:
        loop()
    # When 'Ctrl+C' is pressed
    except KeyboardInterrupt:
        destroy()
