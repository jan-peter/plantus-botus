# Everything about the InfluxDB database

from influxdb import InfluxDBClient

class DB:
    """DataBase class to handle DB things."""

    def __init__(self, logger):
        self.client = self.db_start("test", logger)

    def db_setup(self, arg, logger):
        """Initial setup for the DB."""

        # Setup database
        client = InfluxDBClient(host="localhost", port=8086)

        # To reset the test DB
        if arg == "reset":
            client.drop_database("test")
            logger.warning("A database got dropped.")
        client.create_database("test")
        logger.warning("A database got created.")

    def db_start(self, desired_db_name: str, logger):
        """Setting up the connection to the InfluxDB."""

        client = InfluxDBClient(
            host="localhost", port=8086, username="admin", password="admin"
        )
        client.switch_database(desired_db_name)
        logger.info(f"switched database successfully to {desired_db_name}")
        return client

    def db_write(self, payload: list) -> None:
        """Function to write data to the InfluxDB."""

        # print payload for checking
        print("\nCurrent Pi status:")
        print(payload)

        # write data to DB
        self.client.write_points(payload)
