# Everything about measuring the Raspberry Pi's status and controlling the relay

import subprocess
from time import sleep

import RPi.GPIO as GPIO
import smbus2 as smbus
from psutil import cpu_freq, cpu_percent, disk_partitions, disk_usage, virtual_memory


LEDPin = 11  # RPI Board pin11
RELAY_PIN = 11

# SMBus object -> Filled in setup()
bus = None

# SensorHub constants
DEVICE_BUS = 1
DEVICE_ADDR = 0x17

TEMP_REG = 0x01
LIGHT_REG_L = 0x02
LIGHT_REG_H = 0x03
STATUS_REG = 0x04
ON_BOARD_TEMP_REG = 0x05
ON_BOARD_HUMIDITY_REG = 0x06
ON_BOARD_SENSOR_ERROR = 0x07
BMP280_TEMP_REG = 0x08
BMP280_PRESSURE_REG_L = 0x09
BMP280_PRESSURE_REG_M = 0x0A
BMP280_PRESSURE_REG_H = 0x0B
BMP280_STATUS = 0x0C
HUMAN_DETECT = 0x0D



class Pi:
    """A Pi object is responsible for everything."""

    def __init__(self) -> None:
        self.status = {}

    # https://www.thepythoncode.com/article/get-hardware-system-information-python
    def get_size(self, bytes, suffix="B"):
        """Scale bytes to its proper format e.g:
        1253656 => '1.20MB'
        1253656678 => '1.17GB'
        """

        factor = 1024
        for unit in ["", "K", "M", "G", "T", "P"]:
            if bytes < factor:
                return f"{bytes:.2f}{unit}{suffix}"
            bytes /= factor

    # https://github.com/gsurma/rpi_lcd_system_monitoring/blob/master/rpi_lcd_system_monitoring.py
    def get_temperature(self):

        reading = subprocess.check_output(["/opt/vc/bin/vcgencmd", "measure_temp"])
        reading_string = reading.decode()

        return float(reading_string.split("=")[1][:-3])

    def get_cpu_usage(self):
        return cpu_percent(interval=0.5)

    def get_cpu_frequency(self):
        return cpu_freq()

    def get_disk_usage(self):
        partitions = disk_partitions()
        partition = partitions[0]
        try:
            partition_usage = disk_usage(partition.mountpoint)
            disk_used = self.get_size(partition_usage.used)
            disk_free = self.get_size(partition_usage.free)
            disk_used_percentage = partition_usage.percent
            return disk_used, disk_free, disk_used_percentage
        except PermissionError:
            # this can be catched due to the disk that isn't ready
            pass

    def get_memory_available(self):
        """Return the available memory in MB."""

        svmem = virtual_memory()
        mem_avail = self.get_size(svmem.available)
        return mem_avail

    def update_status(self):
        """Update status information of the Pi."""

        self.status["cpu temperature"] = self.get_temperature()
        self.status["cpu usage"] = self.get_cpu_usage()

        disk_used, disk_free, disk_used_percentage = self.get_disk_usage()
        self.status["disk_used"] = disk_used
        self.status["disk_free"] = disk_free
        self.status["disk_used_percentage"] = disk_used_percentage

        mem_avail = self.get_memory_available()
        self.status["memory_avail"] = mem_avail

    def get_status(self) -> dict:
        return self.status

    def pi_setup(self) -> None:
        """Setting up all the right GPIO pins and bus."""

        global bus
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(LEDPin, GPIO.OUT)
        GPIO.output(LEDPin, GPIO.LOW)  # Set ledPin low to off led

        # Numbers GPIOs by physical location
        # Set ledPin's mode is output
        # print('using pin%d' % LEDPin)

        # SensorHub
        bus = smbus.SMBus(DEVICE_BUS)

    def destroy(self):
        GPIO.output(LEDPin, GPIO.LOW)  # led off
        GPIO.cleanup()  # Release resource

    def relay(self) -> None:
        """Switching the relay on and off for the water pump."""
        GPIO.output(RELAY_PIN, GPIO.HIGH)  # relay on
        print("...relay on")
        sleep(3)  # delay 1 second
        GPIO.output(RELAY_PIN, GPIO.LOW)  # relay off
        print("...relay off")
        sleep(1)  # delay 1 second

    def blink_led(self, ledPin: int) -> None:
        """Making an LED blinkn on GPIO Pin ledPin"""

        GPIO.output(ledPin, GPIO.HIGH)  # led on
        print("...led on")
        sleep(1)  # delay 1 second
        GPIO.output(ledPin, GPIO.LOW)  # led off
        print("led off...")
        sleep(1)

    # SensorHub
    def get_sensorhub_data(self) -> dict:
        """Getting sensor data from the SensorHub."""

        output = {}

        # Setup Buffer
        aReceiveBuf = []
        aReceiveBuf.append(0x00)

        # fill buffer
        for i in range(TEMP_REG, HUMAN_DETECT + 1):
            aReceiveBuf.append(bus.read_byte_data(DEVICE_ADDR, i))

        output["air_temperature"] = aReceiveBuf[ON_BOARD_TEMP_REG]
        output["air_humidity"] = aReceiveBuf[ON_BOARD_HUMIDITY_REG]
        output["sensor_brightness_lux"] = (
            aReceiveBuf[LIGHT_REG_H] << 8 | aReceiveBuf[LIGHT_REG_L]
        )
        output["barometer_temperature"] = aReceiveBuf[BMP280_TEMP_REG]
        output["barometer_pressure"] = (
            aReceiveBuf[BMP280_PRESSURE_REG_L]
            | aReceiveBuf[BMP280_PRESSURE_REG_M] << 8
            | aReceiveBuf[BMP280_PRESSURE_REG_H] << 16
        )

        return output
