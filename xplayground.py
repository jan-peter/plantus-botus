import argparse


def main(argv=None) -> None:
    parser = argparse.ArgumentParser(
        description="Your friendly plant robot."
        "The following shows you what is possible",
        prog="Plantus Botus",
    )
    parser.add_argument(
        "--debug", "-d", action="store_true", help="the debug flag", dest="debug"
    )
    parser.add_argument(
        "---reset", "-r", action="store_true", help="resetting the whole project"
    )

    args = parser.parse_args(argv)

    # print(vars(args))
    # print("debug is: ", args.debug)
    # print("reset is: ", args.reset)

    print(vars(args))


# Program start from here
if __name__ == "__main__":
    exit(main())
    # main()
