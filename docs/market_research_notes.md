# Team Market Research

## Search Keywords
- smart gardening
- automated watering system
- GIY - Grow it Yourself
- indoor gardening
- smart grow

## Ideas for Unique Selling Point (USP) Apporach
- open source -> community driven, transparent, trustworthy, nerdy, modern -> friendly
- versatillity -> every plant possible

## YouTube Watchlist

- Tom: [Lua, the smart planter turning plants into pets!](https://www.youtube.com/watch?v=kAPTF25rn4s&feature=youtu.be)
  - ad video from 2019
  - Plant pot with digital face -> feedback for the plant's condition
  - corresponding [indigogo campaign](https://www.indiegogo.com/projects/lua-the-smart-planter-with-feelings#/)
    - backed €351.150 EUR by 2.775 backers
  - -> **file for bankruptcy** in [June 2021](https://www.indiegogo.com/projects/lua-the-smart-planter-with-feelings#/updates/all)
- Tom: [Smart Plant Pot](https://www.youtube.com/watch?v=LCjydJRffww&feature=youtu.be)
  - November 2022
  - simple demo video - uncommented
  - single plant pot with digital face
  - looks like a hobbyist project
- Tom: [Smart Garten von Simpleplant Fazit nach 8 Wochen](https://www.youtube.com/watch?v=KvX0_zJqsZo)
  - Mai 2022
  - Review von **SmartGarten von Simpleplant**
- Tom: [6 Beste Smart Garden-Systeme und intelligente Pflanzgefäße für den Innenbereich 2021](https://www.youtube.com/watch?v=a8A4U1c1vJ0)
  - Mai 2020
  1. Aero Garden
  2. [GlowPear Mnin](https://glowpear.com/products/glowpear-mini-wall-planter)
  3. [Pico](https://www.indiegogo.com/projects/pico-a-garden-in-your-palm-growing-is-fun-again?utm_source=kickbooster-direct&utm_medium=kickbooster&utm_content=link&utm_campaign=7471f6cf#/updates/all)
    - backed €1.676.106 EUR by 20.674 backers
    - shipped in April 2022
    - light and water
  4. VIVOSUN Hydroponics Growing Kit Indoor Gardening Plant Kit
  5. Greenjoy Indoor Herb Garden Kit
  6. aspara Nature Smart Grower

- Tom: [abby: The All-In-One Smart Hydroponic Grow Box Kickstarter Debut](https://www.youtube.com/watch?v=9YpFFgtBqWw)
- Tom: [SMART GARDEN? How to Automate an Indoor Grow Room Affordably](https://www.youtube.com/watch?v=3-QYAExKQLc)
- Tom: [Automated marijuana smart grow box](https://www.youtube.com/watch?v=GpYnUNiWapM)
- Moe: [Plug and Plant with Leaf](https://www.youtube.com/watch?v=sUfqmjM9_VE)

## Competing products
- Moe: [Hey abby Grow Box](https://heyabby.com/products/abby-automated-in-door-growbox)
- Moe: [Linfa Weezy - Smart Grow Box for Legal Cannabis](https://getlinfa.com/en/products/grow-boxes/linfa-weezy/)
- JP: [smartGarten, simple plant](https://simpleplant.de/collections/smartgarten)

:four_leaf_clover: -> Weed \
:seedling: -> herbs

| Product Name | Type | still in business | price | features | core market | comments |
|----------|----------|----------|----------|----------|----------|----------|
|  Lua    | :seedling:|    no 2019-2021  | TBD | feedback about plant's condition, gamification via digital face, tamagotchi effect, App | single plant lovers  |  simple design, not a comptetion, crowdfunding |
| [smartGarten, simple plant](https://simpleplant.de/collections/smartgarten) | :seedling: | Yes | 200-750 € | App, light,          | kitchen herbs |  German Product, expensive for just growing herbs but luxury design | 
| [Aero Garden](https://www.aerogarden.de/aerogarden-kaufen) | :seedling: |Yes         | 200€+    |          | kitchen herbs | untrusty website, very emotionalised marketing: only we can provide freshness |
|  [GlowPear Mnin](https://glowpear.com/products/glowpear-mini-wall-planter)   |:seedling: | Yes   |  100€+        | self-watering, pvc enclosure  | herbs and small plants, balcony         | calm marketing, our product is just very practical and well though out, USA focused |
| [Pico](https://www.indiegogo.com/projects/pico-a-garden-in-your-palm-growing-is-fun-again?utm_source=kickbooster-direct&utm_medium=kickbooster&utm_content=link&utm_campaign=7471f6cf#/updates/all)  | :seedling: |Yes, 2022 - present  | TBD   | pot for single plant, water & light, USB-C but no battery, **magnetic**        |  kitchen herbs         | only used for 1 herb |
| VIVOSUN Hydroponics Growing Kit Indoor Gardening Plant Kit | :seedling: |TBD | TBD  |  | kitchen herbs & small vegetables, wood and plastic, plastic root cage | interesting company -> weed grow boxes |
| [Greenjoy Indoor Herb Garden Kit](https://iclever.com/de-eu/products/greenjoy-indoor-herb-garden-kit-hydroponics-growing-system) | :seedling: |Yes | 70€ | plastic root cage, water, light, fan | kitchen herbs | |
| [aspara Nature Smart Grower](https://www.grow-green.com/product/aspara-nature-smart-grower-hk/))| :seedling: |Yes | 205€ | App, water, light, root cage, 
| [VIVOSUN GIY 2 x 2 ft. Smart Grow Kit](https://vivosun.com/vivosun-2x2ft-smart-grow-system-complete-grow-kit-24x24x48-with-aerolight-100w-aerozesh-4-inch-p137190735072169005-v137190735072168979) | :four_leaf_clover: | Yes | 550€+ | growbox, ventilation, light | weed grower, App | intersting product, multiple versions, possible collaboration?, USA/Canada focused|
| [Cura](https://www.kickstarter.com/projects/hrbstn/cura-led-grow-light-for-indoor-plants?ref=b5piov&utm_source=Pico+IGG&utm_medium=Update+1)
| [Smart Garden System](https://www.kickstarter.com/projects/sunair/raspberry-pi-smart-garden-system-sgs?ref=nav_search&result=project&term=smart%20garden%20system)|:seedling:


# Legalisinz Canabis
- [Vox: A facted checked debate](https://www.youtube.com/watch?v=8TPaCsQVwA8)

## Acronyms

- TBD - To be defined
